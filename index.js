const express = require('express')
const morgan = require('morgan')
const router = require('./routers')
const {showRequestError} = require('./service')
const app = express()
const PORT = 8080

app.use(express.json())

app.use(morgan('tiny'))

app.use('/api/files', router)

app.use(showRequestError)

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}...`)
})