const fs = require('fs').promises
const path = require('path')

const createFile = async (req, res) => {
    const {filename, content} = req.body

    try {
        await fs.writeFile(`./files/${filename}`, content)
        res.status(200).json({
            message: `${filename} has been created successfully!`
        })
    } catch (err) {
        res.status(500).json({
            message: 'Internal server error!'
        })
    }
}

const deleteFile = async (req, res) => {
    try {
        const fileName = req.params.filename

        await fs.unlink(`./files/${fileName}`)
        res.status(200).json({
            message: `${fileName} has been deleted successfully!`
        })
    } catch (err) {
        res.status(500).json({
            message: 'Internal server error!'
        })
    }
}

const editFile = async (req, res) => {
    try {
        const filename = req.params.filename
        const {content} = req.body

        if (!Object.keys(req.body).length) {
            return res.status(400).json({
                message: 'Request body is empty'
            })
        }

        if(!content) {
            return res.status(400).json({
                message: `Please specify 'content' parameter`
            })
        }

        await fs.writeFile(`./files/${filename}`, content);
        res.status(200).json({
            message: `${filename} has been updated successfully`
        })
    } catch (err) {
        res.status(500).json({
            message: 'Internal server error!'
        })
    }
}

const getFile = async (req, res) => {
    try {
        const fileName = req.params.filename
        const extName = path.extname(fileName).slice(1)
        const fileContent = await fs.readFile(`./files/${fileName}`, 'utf-8')
        const uploadedDate = (await fs.stat(`${__dirname}/files/${fileName}`)).birthtime

        res.status(200).json({
            filename: fileName,
            content: fileContent,
            message: 'Success',
            extension: extName,
            uploadedDate
        })
    } catch (e) {
        res.status(500).json({
            message: 'Internal server error!'
        })
    }
}

const getFiles = async (req, res) => {
    try {
        const allFiles = await fs.readdir(`${__dirname}/files`)

        res.status(200).json({
            message: 'Success',
            files: allFiles
        })
    } catch (e) {
        res.status(500).json({
            message: 'Internal server error!'
        })
    }
}

const checkCreatingFolder = async (req, res, next) => {
    const mainContent = await fs.readdir(`${__dirname}`)
    const isFolderExisting = mainContent.some(item => item === 'files')

    if (!isFolderExisting) {
        await fs.mkdir(`${__dirname}/files`)
    }

    next()
}

const checkCreatingFile = async (req, res, next) => {
    const {filename, content} = req.body
    const allFiles = await fs.readdir(`${__dirname}/files`);
    const isFileExisting = allFiles.find(item => item === filename);

    if(isFileExisting) {
        return res.status(400).json({
            message: `${filename} has already existed`
        })
    }

    if (!req.body || !Object.keys(req.body).length) {
        return res.status(400).json({
            message: 'Request body is empty'
        })
    }

    if(!filename) {
        return res.status(400).json({
            message: `Please specify 'filename' parameter`
        })
    }

    if(!content) {
        return res.status(400).json({
            message: `Please specify 'content' parameter`
        })
    }

    next()
}

const checkFileExtension = async (req, res, next) => {
    const validExtension = /\S+\.(log|txt|json|yaml|xml|js)$/gi
    const fileName = req.params.filename || req.body.filename
    const isValidExtension = validExtension.test(fileName)

    if (!isValidExtension) {
        return res.status(400).json({
            message: 'File extension is invalid'
        })
    }

    next()
}

const checkFileExisting = async (req, res, next) => {
    const fileName = req.params.filename
    const allFiles = await fs.readdir(`${__dirname}/files`)
    const currentFile = allFiles.find(item => item === fileName)

    if (!currentFile) {
        return res.status(400).json({
            message: `${fileName} not found`
        })
    }

    next()
}

const showRequestError = (req, res) => {
    res.status(404).json({
        message: 'Not Found! Please check your request!'
    })
}

module.exports = {
    createFile,
    deleteFile,
    editFile,
    getFile,
    getFiles,
    checkCreatingFolder,
    checkCreatingFile,
    checkFileExtension,
    checkFileExisting,
    showRequestError
}