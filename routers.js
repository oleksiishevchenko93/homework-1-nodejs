const express = require('express')
const router = express.Router()

const {
    createFile,
    deleteFile,
    editFile,
    getFile,
    getFiles,
    checkCreatingFolder,
    checkCreatingFile,
    checkFileExtension,
    checkFileExisting
} = require('./service')

router.post('/', checkCreatingFolder, checkCreatingFile, checkFileExtension, createFile)

router.get('/', checkCreatingFolder, getFiles)

router.get('/:filename', checkCreatingFolder, checkFileExtension, checkFileExisting, getFile)

router.delete('/:filename', checkCreatingFolder, checkFileExtension, checkFileExisting, deleteFile)

router.put('/:filename', checkCreatingFolder, checkFileExtension, checkFileExisting, editFile)

module.exports = router
